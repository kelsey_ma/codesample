/*
 * Name: Kelsey Ma
 * Filename: RST.hpp
 * Date: January 28, 2015
 * Description: This C++ module implements a RST that inserts data and rotates
 *              it accordingly.
 */

#ifndef RST_HPP
#define RST_HPP
#include "BST.hpp"
#include <iostream>
#include <stdlib.h>
# include <stdio.h>

using namespace std;

template <typename Data>
class RST : public BST<Data> {

public:

  /* Insert new data into the RST if it is not already present.
   * Input
   *    item - the data to insert.
   *    return - true if the item was inserted. If the item was already contained in
   *       the tree then return false.
   */ 
  virtual bool insert(const Data& item) {
   BSTNode<Data>* newNode = new BSTNode<Data>(item);
    if(insertBST(newNode)) {
      while((newNode->parent != NULL) && (newNode->parent->priority < newNode->priority)) {
	if(newNode->parent->left == newNode) {
	  rotateRight(newNode->parent, newNode);
	} else {
	  rotateLeft(newNode->parent, newNode);
	}
      }
      return true;
    }
    return false;
  }

private:
  bool insertBST(BSTNode<Data> * toAdd) {
    // If nothing in BST, then make node the root
    if(BST<Data>::root == 0) {
      BST<Data>::root = toAdd;
      BST<Data>::root->priority = (int) rand();
      BST<Data>::isize++;
      return true;
    }
    BSTNode<Data>* current = BST<Data>::root;
    while(current != 0) {
      // Add node to left if less than   
	if(toAdd->data < current->data) {
	  // No child nodes so insert node as left child
	  if(current->left == 0) {
	    current->left = toAdd;
	    toAdd->parent = current;
	    toAdd->priority = (int) rand();
	    BST<Data>::isize++;
	    return true;
	  } else {
	  // Continue going through tree to reach end
	    current = current->left;
	  }
        } 
	// Add node to right if greater than
	else if(current->data < toAdd->data) {
	  // No child nodes so insert node as right child
	  if(current->right == 0) {
	    current->right = toAdd;
	    toAdd->parent = current;
	    toAdd->priority = (int) rand();
            BST<Data>::isize++;
	    return true;
	  } else {
	    // Continue going through three to reach end
	    current = current->right;
	  } 
        }
	else {
      // Node already exists so return false
	  delete toAdd;
	  return false;
	  }
    } 
    return false; 
 }

  /* Perform a 'right rotation' that changes the structure of the tree without
   * interfering with the size, data content, and left->right order of nodes in the RST.
   * This rotation is done by changing the 'left', 'right', and/or 'parent' pointers
   * of a subset of nodes without changing their 'priority' values.
   *
   * Input
   *    par - a parent node in the RST with a left child
   *    child - the left child of 'par'
   *
   * Output
   *   The resulting tree will have 'par' and 'child' in the same order but switched
   *   in level, meaning 'par' will effectively become the right-child of 'child'.
   *
   */
  void rotateRight( BSTNode<Data>* par, BSTNode<Data>* child ) {
    child->parent = par->parent;
     
    // Case where node is not the root
    if(child->parent != NULL) {
       if(child->parent->left == par)
        child->parent->left = child;
      else
        child->parent->right = child;
    } else {
      BST<Data>::root = child;
      BST<Data>::root->parent = NULL;
    }   
      
    par->left = child->right;
    if(par->left != NULL)
      par->left->parent = par;

    child->right = par;
    par->parent = child;
  }

  
  /* Perform a 'left rotation' that changes the structure of the tree without
   * interfering with the size, data content, and left->right order of nodes in the RST.
   * This rotation is done by changing the 'left', 'right', and/or 'parent' pointers
   * of a subset of nodes without changing their 'priority' values.
   *
   * Input
   *    par - a parent node in the RST with a right child
   *    child - the right child of 'par'
   *
   * Output
   *   The resulting tree will have 'par' and 'child' in the same order but switched
   *   in level, meaning 'par' will effectively become the left-child of 'child'.
   *
   */
  void rotateLeft( BSTNode<Data>* par, BSTNode<Data>* child )
  {
    child->parent = par->parent;
    
    // Case where node is not the root
    if(child->parent != NULL) {
       if(child->parent->left == par)
         child->parent->left = child;
       else
        child->parent->right = child;
    } else {
      BST<Data>::root = child;
      BST<Data>::root->parent = NULL;
    }     

    par->right = child->left;
    if(par->right != NULL)
      par->right->parent = par;

    child->left = par;
    par->parent = child;
  }

public:

  /* THIS FUNCTION IS USED FOR TESTING, DO NOT MODIFY
   * 
   * Calls BST::insert so we can add data to test rotateRight and rotateLeft before
   * implementing the fully correct version of RST::insert.
   * 
   */
  bool BSTinsert(const Data& item) {
    
    return BST<Data>::insert(item);
    
  }
  
  /* THIS FUNCTION IS USED FOR TESTING, DO NOT MODIFY
   * 
   * Locates the node for an item and rotates it left or right without changing the
   * left->right order of nodes in the RST
   * Input
   *    item - the Data item to rotate.
   *    leftOrRight - if true then rotateLeft, otherwise rotateRight
   *    output - 0 if item could be found and had a child to rotate with, 1 if not. -1 if
   *         rotation failed for other reasons
   */
  int findAndRotate(const Data& item, bool leftOrRight) {
    
     BSTNode<Data>* current = RST<Data>::root;
     while ( current != 0 ) {
       if ( item < current->data ) {
         current = current->left;
       }
       else if ( current->data < item ) {
         current = current->right;
       }
       else {  // already in the tree
         break;
       }
     }
     
     if (current == 0 or current->data != item) {
       return 1;
     }
     
     if (leftOrRight) {
       if (current->right == 0) {
         return 1;
       }
       BSTNode<Data>* right = current->right;
       rotateLeft(current, current->right);
       
       // make sure we actually do a rotation
       if (current->right == right) {
         return -1;
       }
       
     } else {
       if (current->left == 0) {
         return 1;
       }
       BSTNode<Data>* left = current->left;
       rotateRight(current, current->left);
       
       // make sure we actually do a rotation
       if (current->left == left) {
         return -1;
       }
     }
     return 0;
  }
};
#endif // RST_HPP
