/*
 * Name: Kelsey Ma
 * Filename: BSTNode.hpp
 * Date: January 21, 2015
 * Description: This C++ module implements a BSTNode that initializes a
 *              BSTNode with the given data, returns the successor of 
 *              the node, and overloads the << operator to print
 *              a complete look at the node.
 */

#ifndef BSTNODE_HPP
#define BSTNODE_HPP
#include <iostream>
#include <iomanip>

template<typename Data>
class BSTNode {

public:

  BSTNode<Data>* left;
  BSTNode<Data>* right;
  BSTNode<Data>* parent;
  Data const data;   // the const Data in this node.
  int priority; // Priority of the node

  /** Constructor.  Initialize a BSTNode with the given Data item,
   *  no parent, and no children.
   */
  BSTNode(const Data & d) : data(d) {
    left = right = parent = 0;
  }

  /** Return the successor of this BSTNode in a BST, or 0 if none.
   ** PRECONDITION: this BSTNode is a node in a BST.
   ** POSTCONDITION:  the BST is unchanged.
   ** RETURNS: the BSTNode that is the successor of this BSTNode,
   ** or 0 if there is none.
   */ 
  BSTNode<Data>* successor() {
    BSTNode<Data>* current = this;
    // Node has a right child, need to get left of right child
    if(current->right != 0) {
      current = current->right;
      while(current->left != 0) {
        current = current->left; 
      }
      return current;
    }
    // Node has no right child
    while(current->parent != 0) {
      BSTNode<Data>* parent = current->parent;
      // Is left child of parent
      if(parent->left == current)
        return parent;
      // Is right child of parent
      else
        current = parent;
    }

    return 0;
  }

}; 

/** Overload operator<< to print a BSTNode's fields to an ostream. */
template <typename Data>
std::ostream & operator<<(std::ostream& stm, const BSTNode<Data> & n) {
  stm << '[';
  stm << std::setw(10) << &n;                 // address of the BSTNode
  stm << "; p:" << std::setw(10) << n.parent; // address of its parent
  stm << "; l:" << std::setw(10) << n.left;   // address of its left child
  stm << "; r:" << std::setw(10) << n.right;  // address of its right child
  stm << "; d:" << n.data;                    // its data field
  stm << ']';
  return stm;
}


#endif // BSTNODE_HPP
