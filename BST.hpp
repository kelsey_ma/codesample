/* 
 * Name: Kelsey Ma
 * Filename: BST.hpp
 * Date: January 28, 2015
 * Description: This C++ module implements a BST, including the ability to
 *              insert data, delete all nodes, find a specifc item, return
 *              iterators to the first and after last items, perform inorder
 *              and postorder traversals, and return size.
 */


#ifndef BST_HPP
#define BST_HPP
#include "BSTNode.hpp"
#include "BSTIterator.hpp"
#include <iostream>

template<typename Data>
class BST {

protected:

  /** Pointer to the root of this BST, or 0 if the BST is empty */
  BSTNode<Data>* root;

  /** Number of Data items stored in this BST. */
  unsigned int isize;

public:

  /** define iterator as an aliased typename for BSTIterator<Data>. */
  typedef BSTIterator<Data> iterator;

  /** Default constructor.
      Initialize an empty BST.
   */
  BST() : root(0), isize(0) {  }


  /** Default destructor.
      Delete every node in this BST.
   */ 
  virtual ~BST() {
    deleteAll(root);
  }

  /** Given a reference to a Data item, insert a copy of it in this BST.
   *  Return  true if the item was added to this BST
   *  as a result of this call to insert,
   *  false if an item equal to this one was already in this BST.
   *  Note: This function should use only the '<' operator when comparing
   *  Data items. (should not use >, <=, >=)
   */ 
  virtual bool insert(const Data& item) {
   BSTNode<Data>* toAdd = new BSTNode<Data>(item);
    // If nothing in BST, then make node the root
    if(root == 0) {
      root = toAdd;
      isize++;
      return true;
    }
    BSTNode<Data>* current = root;
    while(current != 0) {
      // Add node to left if less than   
	if(toAdd->data < current->data) {
	  // No child nodes so insert node as left child
	  if(current->left == 0) {
	    current->left = toAdd;
	    toAdd->parent = current;
	    isize++;
	    return true;
	  } else {
	  // Continue going through tree to reach end
	    current = current->left;
	  }
        } 
	// Add node to right if greater than
	else if(current->data < toAdd->data) {
	  // No child nodes so insert node as right child
	  if(current->right == 0) {
	    current->right = toAdd;
	    toAdd->parent = current;
            isize++;
	    return true;
	  } else {
	    // Continue going through three to reach end
	    current = current->right;
	  } 
        }
      // Node already exists so return false
	else {
	  delete toAdd;
	  return false;
        }
    } 
    return false;
  }


  /** Find a Data item in the BST.
   *  Return an iterator pointing to the item, or pointing past
   *  the last node in the BST if not found.
   *  Note: This function should use only the '<' operator when comparing
   *  Data items. (should not use >, <=, >=)
   */ 
  iterator find(const Data& item) const {
    BSTNode<Data>* current = root;
    while(current != 0) {
      // Go to right side of tree if it is larger
      if(current->data < item) {
	current = current->right;
      } else if(item < current->data) {
        // Go to left side of tree if it is smaller
	current = current->left;
      } else {
        return typename BST<Data>::iterator(current);
      }
    }
    return end();
  }

  
  /** Return the number of items currently in the BST.
   */ 
  unsigned int size() const {
    return isize;
  }

  /** Return true if the BST is empty, else false.
   */ 
  bool empty() const {
    if(isize > 0) return false;
    else return true;
  }

  /** Return an iterator pointing to the first item in the BST (not the root).
   */ 
  iterator begin() const {
    return typename BST<Data>::iterator(first(root));
 }

  /** Return an iterator pointing past the last item in the BST.
   */
  iterator end() const {
    return typename BST<Data>::iterator(0);
  }

  /** Perform an inorder traversal of this BST.
   */
  void inorder() const {
    inorder(root);
  }


private:

  /** Recursive inorder traversal 'helper' function */

  /** Inorder traverse BST, print out the data of each node in ascending order.
      Implementing inorder and deleteAll base on the pseudo code is an easy way to get started.
   */ 
  void inorder(BSTNode<Data>* n) const {
    /* Pseudo Code:
      if current node is null: return;
      recursively traverse left sub-tree
      print current node data
      recursively traverse right sub-tree
    */
    if(n == 0)
      return;
    inorder(n->left);
    std::cout << *n << std::endl; 
    inorder(n->right);
  }

  /** Find the first element of the BST
   */ 
  static BSTNode<Data>* first(BSTNode<Data>* root) {
    if(root == 0) return 0;
    while(root->left != 0) root = root->left;
    return root;
  }

  /** do a postorder traversal, deleting nodes
   */ 
  static void deleteAll(BSTNode<Data>* n) {
    /* Pseudo Code:
      if current node is null: return;
      recursively delete left sub-tree
      recursively delete right sub-tree
      delete current node
    */
    if(n == 0)
      return;
    deleteAll(n->left);
    deleteAll(n->right);
    delete n;
  }


 };


#endif //BST_HPP
